#!/usr/bin/env bash

aws s3api list-buckets --query 'Buckets[?starts_with(Name, `mark-cli-bucket-public`) == `true`].Name'