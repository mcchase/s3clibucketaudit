#!/usr/bin/env bash

#aws s3api list-buckets --query 'Buckets[?starts_with(Name, `mark-`) == `true`].Name'

aws s3api list-objects --query 'Buckets[?PublicAccessBlockConfiguration.IgnorePublicAcls == `false`]' --output text
#aws s3api list-buckets --query 'Buckets[?CreationDate == `2019-10-02T19:53:03.000Z`]' --output text


#

# https://aws.amazon.com/blogs/developer/leveraging-the-s3-and-s3api-commands/
# THIS WILL SHOW HOW TO PASS IN THE OUTPUT TO INPUT FOR DELETION AFTER QUERY

#aws s3api list-buckets --query 'Buckets[?starts_with(Name, `awsclitest-`) == `true`].[Name]' --output text | xargs -I {} aws s3 rb s3://{} --force delete: s3://awsclitest-123/test remove_bucket: s3://awsclitest-123/