#!/usr/bin/env bash

##################### GET ALL BUCKETS #################

BUCKETS_LIST=`aws s3api list-buckets --query 'Buckets[*].[Name]' --output text`
for BUCKET_NAME in $BUCKETS_LIST; do
#  echo $BUCKET_NAME
  for BUCKET in $BUCKET_NAME; do
    PUBLIC_BLOCK=`aws s3api get-public-access-block --bucket $BUCKET --output text`
    ACLS=`aws s3api get-bucket-acl --bucket $BUCKET --output text`
    POLICY_STATUS=`aws s3api get-bucket-policy-status --bucket $BUCKET --output text`
#    if [[ $PUBLIC_BLOCK == *"False"* ]] ; then
#    if [[ $PUBLIC_BLOCK == *"False"* ]] || [[ $ALCS == *"WRITE"* ]] ; then
    if [[ ACLS == *"READ"* ]] || [[ ACLS == *"WRITE"* ]] || [[ ACLS == *"FULL_CONTROL" ]] || [[ $POLICY_STATUS == *"false" ]] || [[ $PUBLIC_BLOCK == *"false" ]]; then
#      echo $PUBLIC_BLOCK

      echo $BUCKET "is a publicly accessed bucket"
    else
      echo $BUCKET "is not a publicly accessed bucket"
    fi
   done
done

##################### GET ALL BUCKETS #################

################# LOGIC FOR SINGLE BUCKET ##############
#BUCKET="mark-cli-bucket-public"
#BUCKET1=`aws s3api get-public-access-block --bucket $BUCKET --output text`
#echo $BUCKET1
#if [[ $BUCKET1 == *"False"* ]]; then
#  echo $BUCKET "is a publicly accessed bucket"
#else
#  echo $BUCKET "is not a publicly accessed bucket"
#fi
################# LOGIC FOR SINGLE BUCKET ##############

################# LOGIC FOR SINGLE BUCKET DELETE ##############

#BUCKET="mark-cli-bucket-public"
#BUCKET1=`aws s3api get-public-access-block --bucket $BUCKET --output text`
#echo $BUCKET1
#if [[ $BUCKET1 == *"False"* ]]; then
#  echo $BUCKET "is a publicly accessed bucket and will be deleted"
##  `aws s3api delete-bucket --bucket $BUCKET --region us-east-1`
#else
#  echo $BUCKET "is not a publicly accessed bucket and will not be deleted"
#fi

################# LOGIC FOR SINGLE BUCKET DELETE ##############