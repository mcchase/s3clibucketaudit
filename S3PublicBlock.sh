#!/usr/bin/env bash



aws s3api get-public-access-block --bucket cd-sam-bucket #dnw - objects can be public / Ohio
aws s3api get-public-access-block --bucket cd-sam-weather-bucket # DNW - objects can be public / VIRGINIA
aws s3api get-public-access-block --bucket cd-lab-bucket # WORKS - bucket and objects can be public / Ohio
aws s3api get-public-access-block --bucket davemybucket #WORKS - bucket and objects not public / Ohio
aws s3api get-public-access-block --bucket cd-my-bucket #dnw - Public / Virginia
aws s3api get-public-access-block --bucket mark-cli-bucket-public

#aws s3api get-bucket-acl --bucket mark-cli-bucket-public



